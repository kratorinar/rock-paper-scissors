import React from "react";
import "./homepage.css";
import Coin from "../../components/coin/coin";
import { ReactComponent as Triangle } from "../../assets/bg-triangle.svg";
// import { useSelector, useDispatch } from 'react-redux'
// import { decrementScore, incrementScore, changeMove } from '../../features/game/gameSlice'

function HomePage() {
  const moves = [
    {
      name: "rock",
    },
    {
      name: "paper",
    },
    {
      name: "scissors",
    },
  ];

  // const score = useSelector((state) => state.game.score)
  // const move = useSelector((state) => state.game.move)
  // console.log(score)
  // console.log(move)
  // const dispatch = useDispatch()

  return (
    <div className="game-area">
        {/* <button onClick={() => dispatch(incrementScore())} > Increment score </button>
        <span>{score}</span>
        <button onClick={() => dispatch(decrementScore())} > Decrement </button>

        <br /><br />
        <span>{move}</span>

        <button onClick={() => dispatch(changeMove('test'))} > change move </button> */}
  
        {moves.map(({ name }, index) => (
        <Coin key={index} type={name}></Coin>
      ))}
      <Triangle className="triangle"></Triangle>
    </div>
  );
}

export default HomePage;