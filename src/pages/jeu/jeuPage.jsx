import React from 'react';
import './jeuPage.css';
import Coin from "../../components/coin/coin";
import { useSelector, useDispatch } from 'react-redux'
import { decrementScore, incrementScore } from '../../features/game/gameSlice'
import { Link } from 'react-router-dom'

function JeuPage() {
  const dispatch = useDispatch()
  const player = useSelector((state) => state.game.move)

  const moves = [
      "rock",
      "paper",
      "scissors"
  ];
  const luck = Math.floor(Math.random()*moves.length)
  const ordi = moves[luck]
  let result = ""

  if (
    (player === 'rock' && ordi ==='paper') ||
    (player === 'paper' && ordi ==='scissors') ||
    (player === 'scissors' && ordi ==='rocks')
  ){
    result = "Perdu"
    dispatch(decrementScore())
  } else if (
    (player === 'rock' && ordi ==='scissors') ||
    (player === 'paper' && ordi ==='rock') ||
    (player === 'scissors' && ordi ==='paper')
  ) {
    result = "gagné"
    dispatch(incrementScore())
  } else if ( player === ordi) {
    result = "Egalité"
  }

  return (
    <div className="jeu-page">
      <p class="insights"> joueur : { player }</p>
      <p class="insights"> ordi : { ordi }</p>
      <p class="insights"> result : { result }</p>
      <Coin type={player}/>
      <Coin type={ordi}/>

      <Link className='back-button' to="/">Prochaine manche</Link>
    </div>
  );
}

export default JeuPage;