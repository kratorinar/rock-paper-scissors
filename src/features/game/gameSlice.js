import { createSlice } from '@reduxjs/toolkit'

export const gameSlice = createSlice({
  name: 'game',
  initialState: {
    score: 0,
    move: 'rien'
  },
  reducers: {
    incrementScore: (state) => {
      state.score += 1
    },
    decrementScore: (state) => {
      state.score -= 1
    },
    changeMove: (state, action) => {
      state.move = action.payload
    }
  },
})

// Action creators are generated for each case reducer function
export const { incrementScore, decrementScore, changeMove } = gameSlice.actions

export default gameSlice.reducer